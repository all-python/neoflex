import tldextract
from array import *

#Эта программа вычленяет домен сайта из введеной в консоль ссылки.
def first():
    url = input("URL: ")
    extracted_info = tldextract.extract(url)
    print("domain name =", extracted_info.domain)

#Эта программа создает массив из введенных в консоль чисел, из которого потом выбирает нули и перемещает в конец
def second():
    arr = array("i",[])
    i = 7
    while i > 0:
        try:
            arr.append(int(input("Введите элемент массива: ")))
        except ValueError:
            print("Массив принимает только числовые значения.")
            arr.append(0)
        i -= 1
    print("Ваш массив: ", arr)
    for zero in arr:
        if zero == 0:
            arr.remove(0)
            arr.append(0)
    print("Ваш массив после перемещения нулей: ", arr)

# Эта программа генерирует хештег из введенного в консоль предложения
def third():
    hashtag = input("Введите текст для генерации хештега: ")
    hashtag = hashtag.title()
    hashtag = hashtag.split()
    hashtag = ''.join(hashtag)
    if len(hashtag)<140 and hashtag != "":
        print("#"+hashtag)
    else:
        return False

# Меню для выбора программы
def menu():
    ex = int(input("Тестовое задание №1/2/3: "))
    if ex == 1:
        first()
    elif ex == 2:
        second()
    elif ex == 3:
        third()
    else:
        return False
menu()